# -*- coding: utf-8 -*-
from trytond.report import Report

__all__ = ['ReportAnexo']

class ReportAnexo(Report):
    __name__ = 'gnuhealth.report.anexo'
