# -*- coding: utf-8 -*-

from trytond.pool import Pool
from .health_anexo import *
from .report import *

def register():
    Pool.register(
        Anexo,
        module='health_anexo_fiuner', type_='model')
    Pool.register(
        ReportAnexo,
        module='health_anexo_fiuner', type_='report')
