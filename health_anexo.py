# -*- coding: utf-8 -*-

from trytond.model import ModelView, ModelSingleton, ModelSQL, fields

__all__ = ['Anexo']


class Anexo(ModelSQL, ModelView):
    "Anexo"
    __name__ = "gnuhealth.patient"
